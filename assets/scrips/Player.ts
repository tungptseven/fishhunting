// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class Player extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.runAction(cc.repeatForever(
            cc.sequence(
                cc.moveBy(1, 0, -10),
                cc.moveBy(1, 0, 10)
            )
        ))
    }

    start() {

    }

    // update (dt) {}
}
