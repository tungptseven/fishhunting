// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import GameScreen from "./GameScreen"
import Task from "./Task"
import AudioSourceControl from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class Game extends cc.Component {
    isMuted: boolean = false

    @property(cc.Sprite)
    spGameOver: cc.Sprite = null

    @property(cc.Sprite)
    spGameScreen: cc.Sprite = null

    @property(cc.Sprite)
    spHomeScreen: cc.Sprite = null

    @property(cc.Label)
    pointLabel: cc.Label = null

    @property(cc.Label)
    timeLabel: cc.Label = null

    @property
    time: number = 300

    @property
    point: number = 0

    @property(AudioSourceControl)
    audioSourceControl: AudioSourceControl = null

    // LIFE-CYCLE CALLBACKS:
    addPoint(fishName: string) {
        switch (fishName) {
            case 'ca 1': {
                this.point += 100
                break
            }
            case 'ca 2': {
                this.point += 150
                break
            }
            case 'ca 3': {
                this.point += 160
                break
            }
            case 'ca 4': {
                this.point += 80
                break
            }
            case 'ca 5': {
                this.point += 120
                break
            }
            case 'ca 6': {
                this.point += 90
                break
            }
            case 'ca 7': {
                this.point += 200
                break
            }
        }
        this.pointLabel.string = ' - POINT: ' + this.point.toString()
    }

    onLoad() {
        this.onInit()
    }

    onHit(fish: any) {
        let task = this.spGameScreen.node.getChildByName("Task").getComponent(Task).task
        if (fish.name == task.name) {
            this.addPoint(fish.name)
        }
    }

    onInit() {
        this.spHomeScreen.node.active = true
        this.spGameOver.node.active = false
        this.spGameScreen.node.active = false
        this.audioSourceControl.onStart(this.isMuted)
    }

    onPlay() {
        this.spHomeScreen.node.active = false
        this.spGameScreen.node.active = true
        this.countdown(this.time)
        this.audioSourceControl.onStart(this.isMuted)
        // this.node.getChildByName('ResetBtn').active = true
    }

    onReset() {
        this.spGameOver.node.active = false
        this.spGameScreen.node.active = true
        this.onPlay()
        this.point = 0
        this.pointLabel.string = ' - POINT: ' + this.point.toString()
        this.audioSourceControl.onStart(this.isMuted)
    }

    onSoundToggle() {
        this.isMuted = !this.isMuted
        if (this.isMuted) {
            this.audioSourceControl.pause()
        } else {
            this.audioSourceControl.resume()
        }
    }

    gameOver() {
        this.spGameScreen.node.active = false
        this.spGameOver.node.active = true

        this.spGameOver.node.zIndex = 2
        this.spGameOver.node.getChildByName('HighScore').getChildByName('Point').getComponent(cc.Label).string = this.point.toString()
    }

    countdown(time: number) {
        --time
        this.timeLabel.string = time.toString()
        if (time > 0) {
            this.scheduleOnce(() => this.countdown(time), 1)
        } else {
            this.gameOver()
        }
        this.timeLabel.string = 'TIME: ' + time
    }

    start() {

    }

    update(dt: number) { }
}
