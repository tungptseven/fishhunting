// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const { ccclass, property } = cc._decorator

@ccclass
export default class GameScreen extends cc.Component {

    @property(cc.Node)
    fishPool: cc.Node = null

    @property(cc.Prefab)
    fish1: cc.Prefab = null

    @property(cc.Prefab)
    fish2: cc.Prefab = null

    @property(cc.Prefab)
    fish3: cc.Prefab = null

    @property(cc.Prefab)
    fish4: cc.Prefab = null

    @property(cc.Prefab)
    fish5: cc.Prefab = null

    @property(cc.Prefab)
    fish6: cc.Prefab = null

    @property(cc.Prefab)
    fish7: cc.Prefab = null

    Lfishes: any[] = [this.fish1, this.fish4]
    Rfishes: any[] = [this.fish2, this.fish3, this.fish5, this.fish6, this.fish7]

    // LIFE-CYCLE CALLBACKS:
    initLFish(numb: number) {
        switch (numb) {
            case 0: {
                return cc.instantiate(this.fish1)
            }
            case 1: {
                return cc.instantiate(this.fish4)
            }
        }
    }

    initRFish(numb: number) {
        switch (numb) {
            case 0: {
                return cc.instantiate(this.fish2)
            }
            case 1: {
                return cc.instantiate(this.fish3)
            }
            case 2: {
                return cc.instantiate(this.fish5)
            }
            case 3: {
                return cc.instantiate(this.fish6)
            }
            case 4: {
                return cc.instantiate(this.fish7)
            }
        }
    }

    createLeftFish() {
        let newFish = this.initLFish(Math.floor(Math.random() * this.Lfishes.length))
        const positions = [cc.v2(-800, -50), cc.v2(-800, -200)]
        let fishPosition = Math.floor(Math.random() * positions.length)
        newFish.setPosition(positions[fishPosition])
        this.fishPool.addChild(newFish)

        let actionBy = cc.moveTo(10, cc.v2(-newFish.position.x, newFish.position.y))
        let destruction = cc.callFunc(() => {
            newFish.destroy()
        }, this)
        let sequence = cc.sequence(actionBy, destruction)
        newFish.runAction(sequence)
    }

    createRightFish() {
        let newFish = this.initRFish(Math.floor(Math.random() * this.Rfishes.length))
        const positions = [cc.v2(800, -50), cc.v2(800, -200)]
        let fishPosition = Math.floor(Math.random() * positions.length)
        newFish.setPosition(positions[fishPosition])
        this.fishPool.addChild(newFish)

        let actionBy = cc.moveTo(10, cc.v2(-newFish.position.x, newFish.position.y))
        let destruction = cc.callFunc(() => {
            newFish.destroy()
        }, this)
        let sequence = cc.sequence(actionBy, destruction)
        newFish.runAction(sequence)
    }

    onLoad() {
        this.schedule(() => this.createLeftFish(), 5, cc.macro.REPEAT_FOREVER, 1)
        this.schedule(() => this.createRightFish(), 3, cc.macro.REPEAT_FOREVER, 2)
    }

    start() {
    }

    update(dt) {
    }
}
