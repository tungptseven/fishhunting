// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class Task extends cc.Component {

    @property(cc.JsonAsset)
    fishData: cc.JsonAsset = null

    @property(cc.Label)
    point: cc.Label = null

    @property(cc.Sprite)
    image: cc.Sprite = null

    task: any = null

    // LIFE-CYCLE CALLBACKS:

    initTask() {
        this.task = this.fishData.json[Math.floor(Math.random() * this.fishData.json.length)]
        this.point.string = this.task.point
        let self = this
        let url = this.task.res
        cc.loader.loadRes(url, cc.SpriteFrame, function (err, spriteFrame) {
            if (err) {
                cc.error(err.message || err)
                return
            }
            var node = new cc.Node("New Sprite")
            var sprite = node.addComponent(cc.Sprite)
            sprite.spriteFrame = spriteFrame
            node.parent = self.node.getChildByName('TaskImage')
        })

        let delay = cc.delayTime(14.5)
        let destruction = cc.callFunc(() => {
            this.node.getChildByName('TaskImage').destroyAllChildren()
        }, this)
        let sequence = cc.sequence(delay, destruction)
        this.node.runAction(sequence)
    }

    onLoad() {
        this.schedule(() => this.initTask(), 15, cc.macro.REPEAT_FOREVER, 1)
    }

    start() {

    }

    // update (dt) {}
}
