// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Fish from "./Fish"

const { ccclass, property } = cc._decorator

@ccclass
export default class Hook extends cc.Component {
    drawing: cc.Graphics
    joint: cc.Graphics
    private _isDown: boolean = false

    public get isDown(): boolean {
        return this._isDown
    }

    public set isDown(value: boolean) {
        this._isDown = value
    }

    // LIFE-CYCLE CALLBACKS:

    movingHook(degree, distance) {
        let radian = degree / 360 * 2 * Math.PI
        let deltaX = Math.cos(radian) * distance
        let deltaY = Math.sin(radian) * distance
        return cc.moveBy(1.5, deltaX, deltaY)
    }

    onHook(isCatched: boolean) {
        let action: cc.Action
        // MOVE BY ANGLE
        if (this.isDown) {
            action = this.hookUp(isCatched)
            this.rotateHook()
        } else {
            action = this.hookDown()
        }
        this.isDown = !this.isDown
        this.node.runAction(action)
    }

    hookDown() {
        this.node.getComponent(cc.CircleCollider).enabled = true
        this.node.stopAllActions()
        return this.movingHook(this.node.angle, 600)
    }

    hookUp(isCatched: boolean) {
        this.drawString({ x: 137, y: 243 }, { x: this.node.position.x, y: this.node.position.y })
        this.node.getComponent(cc.CircleCollider).enabled = false
        let delay = cc.delayTime(1)
        let move = cc.moveTo(2, cc.v2(137, 243))
        if (isCatched) {
            return this.node.runAction(cc.sequence(delay, move))
        } else {
            return this.node.runAction(move)
        }
    }

    rotateHook() {
        this.node.runAction(cc.repeatForever(
            cc.sequence(
                cc.rotateTo(1, 45),
                cc.rotateTo(1, 145),
            )
        ))
    }

    drawString(from, to) {
        this.joint = this.node.parent.getChildByName('Joint').getComponent(cc.Graphics)
        this.joint.circle(139, 242, 5)
        this.joint.strokeColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
        this.joint.fillColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
        this.joint.stroke()
        this.joint.fill()
        if (this.isDown) {
            this.drawing = this.node.parent.getChildByName('String').getComponent(cc.Graphics)
            this.drawing.lineWidth = 2
            this.drawing.moveTo(from.x, from.y)
            this.drawing.lineTo(to.x, to.y)
            this.drawing.strokeColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
            this.drawing.fillColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
            this.drawing.stroke()
            this.drawing.fill()
        } else {
            if (this.drawing) {
                this.drawing.clear()
                this.drawing = this.node.parent.getChildByName('String').getComponent(cc.Graphics)
                this.drawing.lineWidth = 2
                this.drawing.moveTo(to.x, to.y)
                this.drawing.lineTo(from.x, from.y)
                this.drawing.strokeColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
                this.drawing.fillColor = cc.Color.set(cc.Color.BLACK, 229, 233, 235)
                this.drawing.stroke()
                this.drawing.fill()
            }
        }
    }

    onLoad() {
        this.rotateHook()
        cc.Canvas.instance.node.getChildByName('GameScreen').on(cc.Node.EventType.TOUCH_START, () => this.onHook(false), this)
        // event.stopPropagation()
        // cc.macro.ENABLE_MULTI_TOUCH = false
    }

    start() {

    }

    update(dt) {
        this.drawString({ x: 137, y: 243 }, { x: this.node.position.x, y: this.node.position.y })
    }
}
