// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Game from "./Game"
import Hook from "./Hook"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class Fish extends cc.Component {
    gameControl: Game = null

    private _isCatched: boolean = false
    collisionManager = cc.director.getCollisionManager()

    public get isCatched(): boolean {
        return this._isCatched
    }

    public set isCatched(value: boolean) {
        if (!this.isCatched) {
            this._isCatched = value
            this.node.stopAllActions()

            this.scheduleOnce(() => {
                this.playDeath()
                this.node.parent.parent.parent.getComponent(Game).onHit(this.node)
            }, 0.3)
        }
    }

    playDeath() {
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
        this.node.runAction(cc.scaleTo(0, 1))
        this.node.getComponent(cc.CircleCollider).enabled = false
        this.node.runAction(cc.moveTo(1.25, cc.v2(137, 243)))
        this.scheduleOnce(() => this.onRemove(), 1.25)
    }

    onRemove() {
        this.node.removeFromParent()
        this.node.destroy()
    }

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(other, self) {
        this.isCatched = true
        this.node.parent.parent.getChildByName('Hook').stopAllActions()
        this.node.parent.parent.getChildByName('Hook').getComponent(Hook).onHook(true)
    }

    onLoad() {
        this.gameControl = cc.Canvas.instance.node.getComponent('Game')
        this.node.runAction(cc.repeatForever(
            cc.sequence(
                cc.rotateTo(0.5, 4),
                cc.rotateTo(0.5, -4)
            )
        ))
        this.collisionManager.enabled = true
        this.collisionManager.enabledDebugDraw = false
    }

    start() {
    }

    // update (dt) {}
}
